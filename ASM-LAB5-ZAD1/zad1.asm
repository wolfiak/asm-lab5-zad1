.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
atoi  PROTO :DWORD
lstrlenA PROTO :DWORD

.DATA
		cout		   dd ?
		cin			   dd ?
		tekst          db "Wprowadz liczbe: ",0
		rozmiart       db $ - tekst
		liczba         dd 0
		liczbaW		   db "Wynik  to: %i ",10,0
		rliczbaW	   dd $ - liczbaW
		bufor          db 128 DUP(?)
		bufor2          db 128 DUP(?)
		komunikat      db "Podane liczby nie sa rowne",0
		rkomunikat     dd $ - komunikat
		komunikat2     db "Podane liczby sa rowne",0
		rkomunikat2    dd $ - komunikat2
		zm             db 0
		zm2            db ?
	
		liczbaZ        dd 0
		rozmiar		   dd ?

		
.CODE
main proc
	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	
	invoke GetStdHandle, STD_INPUT_HANDLE
	mov cin, EAX

	invoke WriteConsoleA, cout, OFFSET tekst, rozmiart, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor2, 8, OFFSET liczbaZ, 0
	lea EBX, bufor2
	mov EDI, liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	invoke atoi, OFFSET bufor2
	mov zm,al

	invoke wsprintfA, OFFSET bufor, OFFSET liczbaW, zm
	invoke WriteConsoleA, cout, OFFSET bufor, rliczbaW, OFFSET liczba, 0 

	invoke WriteConsoleA, cout, OFFSET tekst, rozmiart, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor2, 8, OFFSET liczbaZ, 0
	lea EBX, bufor2
	mov EDI, liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	invoke atoi, OFFSET bufor2
	mov zm2,al


	invoke wsprintfA, OFFSET bufor, OFFSET liczbaW, zm2
	invoke WriteConsoleA, cout, OFFSET bufor, rliczbaW, OFFSET liczba,0 

	mov al,zm
	cmp al, zm2
	jne Rowne
	invoke WriteConsoleA, cout, OFFSET komunikat2, rkomunikat2, OFFSET liczba,0 
	Koniec:

invoke ExitProcess, 0
Rowne:
	invoke WriteConsoleA, cout, OFFSET komunikat, rkomunikat, OFFSET liczba,0 
	jmp Koniec


main endp
atoi proc uses esi edx inputBuffAddr:DWORD
	mov esi, inputBuffAddr
	xor edx, edx
	xor EAX, EAX
	mov AL, BYTE PTR [esi]
	cmp eax, 2dh
	je parseNegative

	.Repeat
		
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0
	mov EAX, EDX
	jmp endatoi

	parseNegative:
	inc esi
	.Repeat
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0

	xor EAX,EAX
	sub EAX, EDX
	jmp endatoi

	endatoi:
	ret
atoi endp
END